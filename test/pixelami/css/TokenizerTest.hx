package pixelami.css;

class TokenizerTest
{
	@Test
	public function shouldTokenizeIdentifierAndCurlyBraces()
	{
		var t = new Tokenizer("foo { bar: baz; }");
		parse(t.tokens);
	}

	@Test
	public function shouldTokenizeFunction()
	{
		var t = new Tokenizer("foo { bar: rgb(255, 0, 127); }");
		parse(t.tokens);
	}

	@Test
	public function shouldTokenizeIDIdentifier()
	{
		var t = new Tokenizer('#foo {}');
		parse(t.tokens);
	}

	@Test
	public function shouldTokenizeAtIdentifier()
	{
		var t = new Tokenizer('@media{ }');
		parse(t.tokens);
	}

	@Test
	public function shouldTokenizeDotIdentifier()
	{
		var t = new Tokenizer('.foo {}');
		parse(t.tokens);
	}

	@Test
	public function shouldCaptureTokenPositions()
	{
		var css = haxe.Resource.getString('css2');
		var t = new Tokenizer(css);

		for (token in t.tokens)
		{
			trace(token + " " + token.pos);
		}
	}

	@Test
	public function shouldCaptureTokenPositions2()
	{
		var css = haxe.Resource.getString('css1');
		var t = new Tokenizer(css);

		for (token in t.tokens)
		{
			//trace(token + " " + token.pos);
		}
	}

	function parse(tokens)
	{
		trace(tokens);
		var p = new Parser(tokens);
		trace(p.stylesheet.toString("   "));
	}
}
