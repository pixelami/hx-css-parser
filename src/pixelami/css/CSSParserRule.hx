package pixelami.css;

import pixelami.css.CSSParserToken;

enum RuleType
{
	Stylesheet;
	AtRule;
	StyleRule;
	Declaration;
	Block;
	Function;
	FunctionArg;
}

enum Mode
{
	TopLevel;
	AtRule;
	Rule;
	Selector;
	Declaration;
	AfterDeclarationName;
	DeclarationValue;
	DeclarationEnd;
	NextBlock;
	NextDeclaration;
}


class CSSParserRule
{
	public var value(default, null):Array<Dynamic>;
	public var fillType(default, null):Mode;
	public var ruleType(default, null):RuleType;
	public var pos:Position;
	public var important:Dynamic;

	public function new()
	{
		fillType = null;
		value = [];
	}

	public function toString(indent)
	{
		return haxe.Json.stringify( this.toJSON() );
	}

	public function append(val:Dynamic)
	{
		value.push(val);
		return this;
	}

	public function toJSON():Dynamic
	{
		return null;
	}
}

class Stylesheet extends CSSParserRule
{
	public function new()
	{
		super();
		ruleType = RuleType.Stylesheet;
	}

	override public function toJSON()
	{
		var v = Lambda.array(Lambda.map(this.value, function(e){ return e.toJSON(); } ));
		return { type:'stylesheet', value: v };
	}
}

class AtRule extends CSSParserRule
{

	static var registry = {
	'import': null,
	'media': Mode.Rule,
	'font-face': Mode.Declaration,
	'page': Mode.Declaration,
	'keyframes': Mode.Rule,
	'namespace': null,
	'counter-style': Mode.Declaration,
	'supports': Mode.Rule,
	'document': Mode.Rule,
	'font-feature-values': Mode.Declaration,
	'viewport': null,
	'region-style': Mode.Rule
	};

	var name:String;
	var prelude:Array<Dynamic>;
	public function new(name)
	{
		super();
		this.name = name;
		this.prelude = [];
		if(Reflect.hasField(registry, name)) this.fillType = Reflect.field(registry, name);
		ruleType = RuleType.AtRule;
	}

	override public function toJSON()
	{
		var pre = Lambda.array(Lambda.map(prelude, function(e){return e.toJSON();}));
		var val = Lambda.array(Lambda.map(value, function(e){return e.toJSON();}));
		return {type:'at', name:this.name, prelude:pre, value:val};
	}

	public function appendPrelude(val)
	{
		this.prelude.push(val);
		return this;
	}
}

class StyleRule extends CSSParserRule
{
	public var selector(default, null):Array<Dynamic>;

	public function new()
	{
		super();
		selector = [];
		ruleType = RuleType.StyleRule;
		fillType = Mode.Declaration;
	}

	public function appendSelector(val)
	{
		this.selector.push(val);
		return this;
	}

	override public function toJSON()
	{
		var sel = Lambda.array(Lambda.map(selector, function(e){return e.toJSON();}));
		var val = Lambda.array(Lambda.map(value, function(e){return e.toJSON();}));
		return {type:'selector', selector:sel, value:val};
	}
}

class Declaration extends CSSParserRule
{
	public var name(default, null):String;
	public function new(name)
	{
		super();
		this.name = name;
		ruleType = RuleType.Declaration;
	}

	override public function toJSON()
	{
		var val = Lambda.array(Lambda.map(value, function(e){ return e.toJSON();}));
		return {type:'declaration', name:this.name, value:val};
	}
}

class SimpleBlock extends CSSParserRule
{
	var type:TokenType;
	public function new(type)
	{
		super();
		this.type = type;
		ruleType = RuleType.Block;
	}

	override public function toJSON()
	{
		var val = Lambda.array(Lambda.map(value, function(e){return e.toJSON();}));
		return {type:'block', value:val};
		// Duplicate field in object declaration : type  from original implementation
		//return haxe.Json.stringify({type:'block', type:this.type, value:val});
	}
}

class Func extends CSSParserRule
{
	var name:String;
	public function new(name:Dynamic)
	{
		super();
		this.name = name.toString();
		ruleType = RuleType.Function;
	}

	override public function toJSON()
	{
		var val = Lambda.array(Lambda.map(value, function(e){return e.toJSON();}));
		return {type:'func', name:this.name, value:val};
	}
}

class FuncArg extends CSSParserRule
{
	public function new()
	{
		super();
		ruleType = RuleType.FunctionArg;
	}

	override public function toJSON()
	{
		return Lambda.array(Lambda.map(value, function(e){ return e.toJSON(); } ));
	}
}