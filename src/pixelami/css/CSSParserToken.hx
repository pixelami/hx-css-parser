package pixelami.css;

enum TokenType {
	BAD_STRING;
	BAD_URL;
	WHITESPACE;
	CDO;
	CDC;
	COLON;
	SEMICOLON;
	CURLY_OPEN;
	CURLY_CLOSE;
	SQUARE_OPEN;
	SQUARE_CLOSE;
	PARENTHESIS_OPEN;
	PARENTHESIS_CLOSE;
	EOF_HX;
	DELIMITER;
	IDENTIFIER;
	FUNCTION;
	AT_KEYWORD;
	HASH;
	STRING;
	URL;
	NUMBER;
	PERCENTAGE;
	DIMENSION;
	UNICODE_RANGE;
}

typedef Position =
{
	char:Int,
	filename:String,
	lineNumber:Int
}

class CSSParserToken
{
	public var value:Dynamic;
	public var tokenType:TokenType;
	public var repr:String;
	public var type:String;
	public var pos:Position;

	function new()
	{}

	public function finish()
	{
		return this;
	}

	public function toString():String
	{
		return Std.string(this.tokenType);
	}

	public function toJSON()
	{
		return toString();
	}

	public function append(val:Dynamic):Bool{ return true; }
}

class BadStringToken extends CSSParserToken
{
	public function new()
	{
		super();
		tokenType = TokenType.BAD_STRING;
	}
}

class BadURLToken extends CSSParserToken
{
	public function new()
	{
		super();
		tokenType = TokenType.BAD_URL;
	}
}

class WhitespaceToken extends CSSParserToken
{
	public function new()
	{
		super();
		tokenType = TokenType.WHITESPACE;
	}

	override function toString()
	{
		return "WS";
	}
}

class CDOToken extends CSSParserToken
{
	public function new()
	{
		super();
		tokenType = TokenType.CDO;
	}
}

class CDCToken extends CSSParserToken
{
	public function new()
	{
		super();
		tokenType = TokenType.CDC;
	}
}

class ColonToken extends CSSParserToken
{
	public function new()
	{
		super();
		tokenType = TokenType.COLON;
	}
}

class SemicolonToken extends CSSParserToken
{
	public function new()
	{
		super();
		tokenType = TokenType.SEMICOLON;
	}
}

class OpenCurlyToken extends CSSParserToken
{
	public function new()
	{
		super();
		tokenType = TokenType.CURLY_OPEN;
	}
}

class CloseCurlyToken extends CSSParserToken
{
	public function new()
	{
		super();
		tokenType = TokenType.CURLY_CLOSE;
	}
}

class OpenSquareToken extends CSSParserToken
{
	public function new()
	{
		super();
		tokenType = TokenType.SQUARE_OPEN;
	}
}

class CloseSquareToken extends CSSParserToken
{
	public function new()
	{
		super();
		tokenType = TokenType.SQUARE_CLOSE;
	}
}

class OpenParentToken extends CSSParserToken
{
	public function new()
	{
		super();
		tokenType = TokenType.PARENTHESIS_OPEN;
	}
}

class CloseParentToken extends CSSParserToken
{
	public function new()
	{
		super();
		tokenType = TokenType.PARENTHESIS_CLOSE;
	}
}

class EOFToken extends CSSParserToken
{
	public function new()
	{
		super();
		tokenType = TokenType.EOF_HX;
	}
}

class DelimiterToken extends CSSParserToken
{
	public function new(code)
	{
		super();
		value = code;
		tokenType = TokenType.DELIMITER;
	}

	override function toString()
	{
		return "DELIM(" + this.value + ")";
	}
}

class StringValuedToken extends CSSParserToken
{
	public function new()
	{
		super();
	}

	override function append(val:Dynamic)
	{
		if(Std.is(val, Array))
		{
			for(i in 0...val.length)
			{
				this.value.push(val[i]);
			}
		}
		else
		{
			this.value.push(val);
		}
		return true;
	}

	override function finish()
	{
		//trace(value);
		this.value = valueAsString();//Tokenizer.stringFromCodeArray(this.value);
		return this;
	}

    public function ASCIIMatch(str:String)
	{
		return this.valueAsString().toLowerCase() == str.toLowerCase();
	}

	public function valueAsString()
	{
	    if(Std.is(value, String)) return value;
		return Tokenizer.stringFromCodeArray(value);
	}

	public function valueAsCodes()
	{
		if(Std.is(value, String))
		{
			var ret = [];
			for(i in 0...value.length)
			{
				ret.push(this.value.charCodeAt(i));
			}
			return ret;
		}
		return this.value.filter(function(e){return e;});
	}
}

class IdentifierToken extends StringValuedToken
{
	public function new(val)
	{
		super();
		value = [];
		this.append(val);
		tokenType = TokenType.IDENTIFIER;
	}

	override function toString()
	{
		return "IDENT(" + this.value + ")";
	}
}

class FunctionToken extends StringValuedToken
{
	public function new(val:Dynamic)
	{
		super();
		// These are always constructed by passing an IdentifierToken
		value = val.finish().value;
		tokenType = TokenType.FUNCTION;
	}

	override function toString()
	{
		return "FUNCTION(" + this.value + ")";
	}
}

class AtKeywordToken extends StringValuedToken
{
	public function new(val:Dynamic)
	{
		super();
		value = [];
		this.append(val);
		tokenType = TokenType.AT_KEYWORD;
	}

	override function toString()
	{
		return "AT(" + this.value + ")";
	}
}

class HashToken extends StringValuedToken
{
	public function new(val:Dynamic)
	{
		super();
		value = [];
		this.append(val);
		tokenType = TokenType.HASH;
	}

	override function toString()
	{
		return "HASH(" + this.value + ")";
	}
}

class StringToken extends StringValuedToken
{
	public function new(?val = "")
	{
		super();
		value = [];
		if(val != "") this.append(val);
		tokenType = TokenType.STRING;
	}

	override function toString()
	{
		return "\"" + this.value + "\"";
	}
}

class URLToken extends StringValuedToken
{
	public function new(?val = null)
	{
		super();
		value = [];
		if(val != null) this.append(val);
		tokenType = TokenType.URL;
	}

	override function toString()
	{
		return "URL(" + this.value + ")";
	}
}

class NumberToken extends StringValuedToken
{
	public function new(?val = null)
	{
		super();
		value = [];
		this.type = "integer";
		if(val != null) this.append(val);
		tokenType = TokenType.NUMBER;
	}

	override function toString()
	{
		if(this.type == "integer") return "INT(" + this.value + ")";
		return "NUMBER(" + this.value + ")";
	}

	override function finish()
	{
		this.repr = valueAsString();//Tokenizer.stringFromCodeArray(this.value);
		this.value = Std.parseInt(this.repr);
		if(value == null)
		{
			this.type = "number";
			this.value = Std.parseFloat(this.repr);
		}
		return this;
	}

	override public function append(val:Dynamic)
	{
		return super.append(val);
	}
}

class PercentageToken extends CSSParserToken
{
	public function new(val)
	{
		super();
		// These are always created by passing a NumberToken as val
		val.finish();
		value = val.value;
		repr = val.repr;
		tokenType = TokenType.PERCENTAGE;
	}

	override function toString()
	{
		return "PERCENTAGE(" + this.value + ")";
	}
}

class DimensionToken extends CSSParserToken
{
	var num:Dynamic;
	var unit:Dynamic;

	public function new(val:NumberToken, unit:Dynamic)
	{
		super();
		// These are always created by passing a NumberToken as val
		val.finish();
		num = val.value;
		this.unit = [];
		repr = val.repr;
		this.append(unit);
		tokenType = TokenType.DIMENSION;
	}

	override function toString()
	{
		return "DIM(" + this.num + "," + this.unit + ")";
	}

	override function append(val):Bool
	{
		if(Std.is(val, Array))
		{
			var a:Array<Int> = cast val;
			for(i in 0...a.length)
			{
				this.unit.push(a[i]);
			}
		}
		else
		{
			this.unit.push(val);
		}
		return true;
	}

	override function finish()
	{
		this.unit = Tokenizer.stringFromCodeArray(this.unit);
		this.repr += this.unit;
		return this;
	}
}

class UnicodeRangeToken extends CSSParserToken
{
	var start:Int;
	var end:Int;
	var code:Int;

	public function new(start:Array<Int>, ?end:Array<Int> = null)
	{
		super();
		// start and end are array of char codes, completely finished
		this.start = Std.parseInt("0x" + Tokenizer.stringFromCodeArray(start));
		if(end == null) this.end = this.start + 1;
		else this.end = Std.parseInt("0x" + Tokenizer.stringFromCodeArray(end));

		if(this.start > Tokenizer.maximumAllowedCodePoint) this.end = this.start;
		if(this.end < this.start) this.end = this.start;
		if(this.end > Tokenizer.maximumAllowedCodePoint) this.end = Tokenizer.maximumAllowedCodePoint;

		tokenType = TokenType.UNICODE_RANGE;
	}

	override function toString()
	{
		if(this.start + 1 == this.end)
			return "UNICODE-RANGE(" + StringTools.hex(this.start).toUpperCase() + ")";
		if(this.start < this.end)
			return "UNICODE-RANGE(" + StringTools.hex(this.start).toUpperCase() + "-" + StringTools.hex(this.end).toUpperCase() + ")";
		return "UNICODE-RANGE()";
	}

	function contains()
	{
		return code >= this.start && code < this.end;
	}
}

