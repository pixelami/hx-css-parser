package pixelami.css;

import StringTools;
import pixelami.css.Tokenizer;
import pixelami.css.CSSParserToken;
import Lambda;


enum State
{
	Data;
	DoubleQuoteString;
	SingleQuoteString;
	Hash_;
	HashRest;
	Comment;
	AtKeyword;
	AtKeywordRest;
    Ident;
    IdentRest;
    TransformFunctionWhitespace;
    Number_;
    NumberRest;
    NumberFraction;
    Dimension;
	SciNotation;
	Url;
	UrlDoubleQuote;
	UrlSingleQuote;
	UrlEnd;
	UrlUnquoted;
	BadUrl;
	UnicodeRange;
}

class Tokenizer
{
	public static function stringFromCodeArray(arr:Array<Int>):String
	{
		var sb = new StringBuf();
		if(arr == null) return sb.toString();
		for(i in 0...arr.length)
		{
			sb.add(String.fromCharCode(arr[i]));
		}
		return sb.toString();
	}

	static function isBetween(num:Int, first:Int, last:Int):Bool
	{
		return num >= first && num <= last;
	}

	static function isDigit(code:Int):Bool
	{
		return isBetween(code, 0x30, 0x39);
	}

	static function isHexDigit(code:Int):Bool
	{
		return isDigit(code) || isBetween(code, 0x41, 0x46) || isBetween(code, 0x61, 0x66);
	}

	static function isUppercaseLetter(code:Int):Bool
	{
		return isBetween(code, 0x41, 0x5a);
	}

	static function isLowercaseLetter(code:Int):Bool
	{
		return isBetween(code, 0x61, 0x7a);
	}

	static function isLetter(code:Int):Bool
	{
		return isUppercaseLetter(code) || isLowercaseLetter(code);
	}

	static function isNonAscii(code:Int):Bool
	{
		return code >= 0xa0;
	}

	static function isNameStartChar(code:Int):Bool
	{
		return isLetter(code) || isNonAscii(code) || code == 0x5f;
	}

	static function isNameChar(code:Int):Bool
	{
		return isNameStartChar(code) || isDigit(code) || code == 0x2d;
	}

	static function isNonPrintable(code:Int):Bool
	{
		return isBetween(code, 0, 8) || isBetween(code, 0xe, 0x1f) || isBetween(code, 0x7f, 0x9f);
	}

	static function isNewLine(code:Int):Bool
	{
		return code == 0xa || code == 0xd;
	}

	static function isWhitespace(code:Int):Bool
	{
		return isNewLine(code) || code == 9 || code == 0x20;
	}

	static function isBadEscape(code:Int):Bool
	{
		return isNewLine(code) || Math.isNaN(code);
	}

	public static inline var maximumAllowedCodePoint = 0x10ffff;

	var i:Int;
	public var tokens(default, null):Array<CSSParserToken>;
	var state:State;
	var code:Dynamic;
	var currentToken:CSSParserToken;
	var options:Dynamic;
	var str:String;

	var currentLineNumber:Int;
	var previousEndOfLine:Int;


	public function new(str:String, ?options:Dynamic = null)
	{
		this.str = str;
		currentLineNumber = 1;
		previousEndOfLine = 1;
		if(options == null) options = {transformFunctionWhitespace:false, scientificNotation:false};
		tokens = [];
		i = -1;
		state = State.Data;
		tokenize();
	}

	function next(?num = null)
	{
		if(num == null) num = 1;
		return str.charCodeAt(i + num);
	}

	function consume(?num:Int = null)
	{
		if(num == null) num = 1;

		i = i + num;
		code = str.charCodeAt(i);

		if(isNewLine(code))
		{
			currentLineNumber++;
			previousEndOfLine = i;
		}

		return true;
	}

	function reconsume()
	{
		i -= 1;
		return true;
	}

	function eof()
	{
		return i >= str.length;
	}

	function doNothing()
	{}

	function emit(?token:CSSParserToken = null)
	{
		if(token != null)
		{
			token.finish();
		}
		else
		{
			token = currentToken.finish();
		}

		tokens.push(token);
		currentToken = null;
		return true;
	}

	function create(token)
	{
		currentToken = token;

		token.pos = {
			filename:null,
			char:i - previousEndOfLine,
			lineNumber:currentLineNumber
		};

		return true;
	}

	function parseError()
	{
        trace("Parse error at index " + i + ", processing codepoint 0x" + StringTools.hex(code) + " in state " + state + ".");
        return true;
	}

	function catchFire(msg:String)
	{
		trace("MAJOR SPEC ERROR: "+msg);
		return true;
	}

	function switchTo(newState:State)
	{
		state = newState;
		return true;
	}

	function consumeEscape():Dynamic
	{
		// Assume the the current character is the \
		consume();
		if(isHexDigit(code))
		{
			// Consume 1-6 hex digits
			var digits = [];
			for(total in 0...6)
			{
				if(isHexDigit(code))
				{
					digits.push(code);
					consume();
				}
				else
				{
					break;
				}
			}
			var chars = Lambda.map(digits, String.fromCharCode).join('');
			var value = Std.parseInt("0x" + chars);
			if(value > maximumAllowedCodePoint) value = 0xfffd;
			// If the current char is whitespace, cool, we'll just eat it.
			// Otherwise, put it back.
			if(!isWhitespace(code)) reconsume();
			return value;
		}
		else
		{
			return code;
		}
	}

	public function tokenize()
	{
		while(_tokenize())
		{

		}
	}

	function _tokenize():Bool
	{
		if(i > str.length * 2)
		{
			throw "I'm infinite-looping!";
		}
		consume();
		switch(state)
		{
			case State.Data:
				if(isWhitespace(code))
				{
					emit(new WhitespaceToken());
					while(isWhitespace(next())) consume();
				}
				else if(code == 0x22) switchTo(State.DoubleQuoteString);
				else if(code == 0x23) switchTo(State.Hash_);
				else if(code == 0x27) switchTo(State.SingleQuoteString);
				else if(code == 0x28) emit(new OpenParentToken());
				else if(code == 0x29) emit(new CloseParentToken());
				else if(code == 0x2b)
				{
					if(isDigit(next()) || (next() == 0x2e && isDigit(next(2)))) switchTo(State.Number_) && reconsume();
					else emit(new DelimiterToken(code));
				}
				else if(code == 0x2d)
				{
					if(next(1) == 0x2d && next(2) == 0x3e) consume(2) && emit(new CDCToken());
					else if(isDigit(next()) || (next(1) == 0x2e && isDigit(next(2)))) switchTo(State.Number_) && reconsume();
					else switchTo(State.Ident) && reconsume();
				}
				else if(code == 0x2e)
				{
					if(isDigit(next())) switchTo(State.Number_) && reconsume();
					else emit(new DelimiterToken(code));
				}
				else if(code == 0x2f)
				{
					if(next() == 0x2a) consume() && switchTo(State.Comment);
					else emit(new DelimiterToken(code));
				}
				else if(code == 0x3a) emit(new ColonToken());
				else if(code == 0x3b) emit(new SemicolonToken());
				else if(code == 0x3c)
				{
					if(next(1) == 0x21 && next(2) == 0x2d && next(3) == 0x2d) consume(3) && emit(new CDOToken());
					else emit(new DelimiterToken(code));
				}
				else if(code == 0x40) switchTo(State.AtKeyword);
				else if(code == 0x5b) emit(new OpenSquareToken());
				else if(code == 0x5c)
				{
					if(isBadEscape(next())) parseError() && emit(new DelimiterToken(code));
					else switchTo(State.Ident) && reconsume();
				}
				else if(code == 0x5d) emit(new CloseSquareToken());
				else if(code == 0x7b) emit(new OpenCurlyToken());
				else if(code == 0x7d) emit(new CloseCurlyToken());
				else if(isDigit(code)) switchTo(State.Number_) && reconsume();
				else if(code == 0x55 || code == 0x75)
				{
					if(next(1) == 0x2b && isHexDigit(next(2))) consume() && switchTo(State.UnicodeRange);
					else switchTo(State.Ident) && reconsume();
				}
				else if(isNameStartChar(code)) switchTo(State.Ident) && reconsume();
				else if(eof())
				{
					emit(new EOFToken());
					return false; //tokens;
				}
				else emit(new DelimiterToken(code));


			case State.DoubleQuoteString:
				if(currentToken == null) create(new StringToken());

				if(code == 0x22) emit() && switchTo(State.Data);
				else if(eof()) parseError() && emit() && switchTo(State.Data) && reconsume();
				else if(isNewLine(code)) parseError() && emit(new BadStringToken()) && switchTo(State.Data) && reconsume();
				else if(code == 0x5c)
				{
					if(isBadEscape(next())) parseError() && emit(new BadStringToken()) && switchTo(State.Data);
					else if(isNewLine(next())) consume();
					else currentToken.append(consumeEscape());
				}
				else currentToken.append(code);


			case State.SingleQuoteString:
				if(currentToken == null) create(new StringToken());

				if(code == 0x27) emit() && switchTo(State.Data);
				else if(eof()) parseError() && emit() && switchTo(State.Data);
				else if(isNewLine(code)) parseError() && emit(new BadStringToken()) && switchTo(State.Data) && reconsume();
				else if(code == 0x5c)
				{
					if(isBadEscape(next())) parseError() && emit(new BadStringToken()) && switchTo(State.Data);
					else if(isNewLine(next())) consume();
					else currentToken.append(consumeEscape());
				}
				else currentToken.append(code);


			case State.Hash_:
				if(isNameChar(code)) create(new HashToken(code)) && switchTo(State.HashRest);
				else if(code == 0x5c)
				{
					if(isBadEscape(next())) parseError() && emit() && switchTo(State.Data) && reconsume();
					else create(new HashToken(consumeEscape())) && switchTo(State.HashRest);
				}
				else emit(new DelimiterToken(0x23)) && switchTo(State.Data) && reconsume();


			case State.HashRest:
				if(isNameChar(code)) currentToken.append(code);
				else if(code == 0x5c)
				{
					if(isBadEscape(next())) parseError() && emit(new DelimiterToken(0x23)) && switchTo(State.Data) && reconsume();
					else currentToken.append(consumeEscape());
				}
				else emit() && switchTo(State.Data) && reconsume();


			case State.Comment:
				if(code == 0x2a)
				{
					if(next() == 0x2f) consume() && switchTo(State.Data);
					else doNothing();
				}
				else if(eof()) parseError() && switchTo(State.Data) && reconsume();
				else doNothing();


			case State.AtKeyword:
				if(code == 0x2d)
				{
					if(isNameStartChar(next())) create(new AtKeywordToken(0x2d)) && switchTo(State.AtKeywordRest);
					else if(next(1) == 0x5c &&  !isBadEscape(next(2))) create(new AtKeywordToken(0x2d)) && switchTo(State.AtKeywordRest);
					else parseError() && emit(new DelimiterToken(0x40)) && switchTo(State.Data) && reconsume();
				}
				else if(isNameStartChar(code)) create(new AtKeywordToken(code)) && switchTo(State.AtKeywordRest);
				else if(code == 0x5c)
				{
					if(isBadEscape(next())) parseError() && emit(new DelimiterToken(0x23)) && switchTo(State.Data) && reconsume();
					else create(new AtKeywordToken(consumeEscape())) && switchTo(State.AtKeywordRest);
				}
				else emit(new DelimiterToken(0x40)) && switchTo(State.Data) && reconsume();


			case State.AtKeywordRest:
				if(isNameChar(code)) currentToken.append(code);
				else if(code == 0x5c)
				{
					if(isBadEscape(next())) parseError() && emit() && switchTo(State.Data) && reconsume();
					else currentToken.append(consumeEscape());
				}
				else emit() && switchTo(State.Data) && reconsume();


			case State.Ident:
				if(code == 0x2d)
				{
					if(isNameStartChar(next())) create(new IdentifierToken(code)) && switchTo(State.IdentRest);
					else if(next(1) == 0x5c && !isBadEscape(next(2))) create(new IdentifierToken(code)) && switchTo(State.IdentRest);
					else emit(new DelimiterToken(0x2d)) && switchTo(State.Data);// && reconsume();
				}
				else if(isNameStartChar(code)) create(new IdentifierToken(code)) && switchTo(State.IdentRest);
				else if(code == 0x5c)
				{
					if(isBadEscape(next())) parseError() && switchTo(State.Data) && reconsume();
					else create(new IdentifierToken(consumeEscape())) && switchTo(State.IdentRest);
				}
				else switchTo(State.Data) && reconsume();


			case State.IdentRest:
				if(isNameChar(code)) currentToken.append(code);
				else if(code == 0x5c)
				{
					if(isBadEscape(next())) parseError() && emit() && switchTo(State.Data) && reconsume();
					else currentToken.append(consumeEscape());
				}
				else if(code == 0x28)
				{
					if(cast(currentToken, StringValuedToken).ASCIIMatch('url')) switchTo(State.Url);
					else emit(new FunctionToken(currentToken)) && switchTo(State.Data);
				}
				else if(isWhitespace(code) && options != null && Reflect.hasField(options, "transformFunctionWhitespace")) switchTo(State.TransformFunctionWhitespace) && reconsume();
				else emit() && switchTo(State.Data) && reconsume();


			case State.TransformFunctionWhitespace:
				if(isWhitespace(next())) doNothing();
				else if(code == 0x28) emit(new FunctionToken(currentToken)) && switchTo(State.Data);
				else emit() && switchTo(State.Data) && reconsume();


			case State.Number_:
				create(new NumberToken());
				if(code == 0x2d)
				{
					if(isDigit(next())) consume() && currentToken.append([0x2d, code]) && switchTo(State.NumberRest);
					else if(next(1) == 0x2e && isDigit(next(2))) consume(2) && currentToken.append([0x2d, 0x2e, code]) && switchTo(State.NumberFraction);
					else switchTo(State.Data) && reconsume();
				}
				else if(code == 0x2b)
				{
					if(isDigit(next())) consume() && currentToken.append([0x2b, code]) && switchTo(State.NumberRest);
					else if(next(1) == 0x2e && isDigit(next(2))) consume(2) && currentToken.append([0x2b, 0x2e, code]) && switchTo(State.NumberFraction);
					else switchTo(State.Data) && reconsume();
				}
				else if(isDigit(code)) currentToken.append(code) && switchTo(State.NumberRest);
				else if(code == 0x2e)
				{
					if(isDigit(next())) consume() && currentToken.append([0x2e, code]) && switchTo(State.NumberFraction);
					else switchTo(State.Data) && reconsume();
				}
				else switchTo(State.Data) && reconsume();


			case State.NumberRest:
				if(isDigit(code)) currentToken.append(code);
				else if(code == 0x2e)
				{
					if(isDigit(next())) consume() && currentToken.append([0x2e, code]) && switchTo(State.NumberFraction);
					else emit() && switchTo(State.Data) && reconsume();
				}
				else if(code == 0x25) emit(new PercentageToken(currentToken)) && switchTo(State.Data);// && reconsume();
				else if(code == 0x45 || code == 0x65)
				{
					if(isDigit(next())) consume() && currentToken.append([0x25, code]) && switchTo(State.SciNotation);
					else if((next(1) == 0x2b || next(1) == 0x2d) && isDigit(next(2))) currentToken.append([0x25, next(1), next(2)]) && consume(2) && switchTo(State.SciNotation);
					else create(new DimensionToken(cast currentToken, code)) && switchTo(State.Dimension);
				}
				else if(code == 0x2d)
				{
					if(isNameStartChar(next())) consume() && create(new DimensionToken(cast currentToken, [0x2d, code])) && switchTo(State.Dimension);
					else if(next(1) == 0x5c && isBadEscape(next(2))) parseError() && emit() && switchTo(State.Data) && reconsume();
					else if(next(1) == 0x5c) consume() && create(new DimensionToken(cast currentToken, [0x2d, consumeEscape()])) && switchTo(State.Dimension);
					else emit() && switchTo(State.Data) && reconsume();
				}
				else if(isNameStartChar(code))
				{
					create(new DimensionToken(cast currentToken, code)) && switchTo(State.Dimension);
				}
				else if(code == 0x5c)
				{
					if(isBadEscape(next())) parseError() && emit() && switchTo(State.Data) && reconsume();
					else create(new DimensionToken(cast currentToken, consumeEscape())) && switchTo(State.Dimension);
				}
				else emit() && switchTo(State.Data) && reconsume();


			case State.NumberFraction:
				currentToken.type = "number";

				if(isDigit(code)) currentToken.append(code);
				else if(code == 0x25) emit(new PercentageToken(currentToken)) && switchTo(State.Data);// && reconsume();
				else if(code == 0x45 || code == 0x65)
				{
					if(isDigit(next())) consume() && currentToken.append([0x25, code]) && switchTo(State.SciNotation);
					if((next(1) == 0x2b || next(1) == 0x2d) && isDigit(next(2))) currentToken.append([0x65, next(1), next(2)]) && consume(2) && switchTo(State.SciNotation);
					else
					{
						create(new DimensionToken(cast currentToken, code)) && switchTo(State.Dimension);
					}
				}
				else if(code == 0x2d)
				{
					if(isNameStartChar(next())) consume() && create(new DimensionToken(cast currentToken, [0x2d, code])) && switchTo(State.Dimension);
					else if(next(1) == 0x5c && isBadEscape(next(2))) parseError() && emit() && switchTo(State.Data) && reconsume();
					else if(next(1) == 0x5c) consume() && create(new DimensionToken(cast currentToken, [0x2d, consumeEscape()])) && switchTo(State.Dimension);
					else emit() && switchTo(State.Data) && reconsume();
				}
				else if(isNameStartChar(code)) create(new DimensionToken(cast currentToken, code)) && switchTo(State.Dimension);
				else if(code == 0x5c)
				{
					if(isBadEscape(next())) parseError() && emit() && switchTo(State.Data) && reconsume();
					else create(new DimensionToken(cast currentToken, consumeEscape())) && switchTo(State.Dimension);
				}
				else emit() && switchTo(State.Data) && reconsume();


			case State.Dimension:
				if(isNameChar(code)) currentToken.append(code);
				else if(code == 0x5c)
				{
					if(isBadEscape(next())) parseError() && emit() && switchTo(State.Data) && reconsume();
					else currentToken.append(consumeEscape());
				}
				else emit() && switchTo(State.Data) && reconsume();


			case State.SciNotation:
				currentToken.type = "number";
				if(isDigit(code)) currentToken.append(code);
				else emit() && switchTo(State.Data) && reconsume();


			case State.Url:
				if(eof()) parseError() && emit(new BadURLToken()) && switchTo(State.Data);
				else if(code == 0x22) switchTo(State.UrlDoubleQuote);
				else if(code == 0x27) switchTo(State.UrlSingleQuote);
				else if(code == 0x29) emit(new URLToken()) && switchTo(State.Data);
				else if(isWhitespace(code)) doNothing();
				else switchTo(State.UrlUnquoted) && reconsume();


			case State.UrlDoubleQuote:
				if(currentToken == null) create(new URLToken());

				if(code == 0x22) switchTo(State.UrlEnd);
				else if(isNewLine(code)) parseError() && switchTo(State.BadUrl);
				else if(code == 0x5c)
				{
					if(isNewLine(next())) consume();
					else if(isBadEscape(next())) parseError() && emit(new BadURLToken()) && switchTo(State.Data) && reconsume();
					else currentToken.append(consumeEscape());
				}
				else currentToken.append(code);


			case State.UrlSingleQuote:
				if(!Std.is(currentToken, URLToken)) create(new URLToken());
				if(eof()) parseError() && emit(new BadURLToken()) && switchTo(State.Data);
				else if(isNewLine(code)) parseError() && switchTo(State.BadUrl);
				else if(code == 0x5c)
				{
					if(isNewLine(next())) consume();
					else if(isBadEscape(next())) parseError() && emit(new BadURLToken()) && switchTo(State.Data) && reconsume();
					else currentToken.append(consumeEscape());
				}
				else currentToken.append(code);


			case State.UrlEnd:
				if(eof()) parseError() && emit(new BadURLToken()) && switchTo(State.Data);
				else if(isWhitespace(code)) doNothing();
				else if(code == 0x29) emit() && switchTo(State.Data);
				else parseError() && switchTo(State.BadUrl) && reconsume();


			case State.UrlUnquoted:
				if(!Std.is(currentToken, URLToken)) create(new URLToken());
				if(eof()) parseError() && emit(new BadURLToken()) && switchTo(State.Data);
				else if(isWhitespace(code)) switchTo(State.UrlEnd);
				else if(code == 0x29) emit() && switchTo(State.Data);
				else if(code == 0x22 || code == 0x27 || code == 0x28 || isNonPrintable(code)) parseError() && switchTo(State.BadUrl);
				else if(code == 0x5c)
				{
					if(isBadEscape(next())) parseError() && switchTo(State.BadUrl);
					else currentToken.append(consumeEscape());
				}
				else currentToken.append(code);


			case State.BadUrl:
				if(eof()) parseError() && emit(new BadURLToken()) && switchTo(State.Data);
				else if(code == 0x29) emit(new BadURLToken()) && switchTo(State.Data);
				else if(code == 0x5c)
				{
					if(isBadEscape(next())) doNothing();
					else consumeEscape();
				}
				else doNothing();


			case State.UnicodeRange:
				// We already know that the current code is a hexdigit.
				var start:Array<Int> = [code];
				var end:Array<Int> = [code];
				var total = 1;
				for(i in total...6)
				{
					total = i;
					if(isHexDigit(next()))
					{
						consume();
						start.push(code);
						end.push(code);
					}
					else return true;
				}

				if(next() == 0x3f)
				{
					for(i in total...6)
					{
						total = i;
						if(next() == 0x3f)
						{
							consume();
							start.push("0".charCodeAt(0));
							end.push("f".charCodeAt(0));
						}
						else return true;
					}
					emit(new UnicodeRangeToken(start, end)) && switchTo(State.Data);
				}
				else if(next(1) == 0x2d && isHexDigit(next(2)))
				{
					consume();
					consume();
					end = [code];
					for(total in 1...6)
					{
						if(isHexDigit(next()))
						{
							consume();
							end.push(code);
						}
						else return true;
					}
					emit(new UnicodeRangeToken(start, end)) && switchTo(State.Data);
				}
				else emit(new UnicodeRangeToken(start)) && switchTo(State.Data);

			#if !haxe3
			default:
				catchFire("Unknown state '" + state + "'");
			#end

		}
		return true;
	}
}

