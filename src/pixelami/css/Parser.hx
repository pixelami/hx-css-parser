package pixelami.css;

import pixelami.css.CSSParserToken;
import pixelami.css.CSSParserRule;
import pixelami.css.Tokenizer;

class Parser
{
	var mode:Mode;
	var i:Int;
	var token:CSSParserToken;

	public var stylesheet(default, null):Stylesheet;
	var stack:Array<Dynamic>;
	var rule:CSSParserRule;
	var tokens:Array<Dynamic>;


	public function new(tokens)
	{
		mode = Mode.TopLevel;
		stylesheet = new Stylesheet();
		stack = [stylesheet];
		rule = stack[0];
		this.tokens = tokens;
		i = -1;
		parse();
	}

	function consume(?advance:Int = null)
	{
		if (advance == null) advance = 1;
		i += advance;
		if (i < tokens.length)
			token = tokens[i];
		else
			token = new EOFToken();
		return true;
	}

	function reprocess()
	{
		i--;
		return true;
	}

	function next()
	{
		return tokens[i + 1];
	}

	function switchTo(?newMode:Mode = null)
	{
		if (newMode == null)
		{
			if (rule.fillType != null)
			{
				mode = rule.fillType;
			}
			else if (rule.ruleType == RuleType.Stylesheet)
			{
				mode = Mode.TopLevel;
			}
			else
			{
				trace("Unknown rule-type while switching to current rule's content mode: ", rule); mode = null;
			}

		}
		else
		{
			mode = newMode;
		}
		return true;
	}

	function push(newRule:CSSParserRule)
	{
		rule = newRule;
		rule.pos = token.pos;
		stack.push(rule);
		return true;
	}

	function parseError(?msg:String = "")
	{
		trace("Parse error at token " + i + ": " + token + ".\n" + msg);
		return true;
	}

	function pop()
	{
		var oldRule = stack.pop();
		rule = stack[stack.length - 1];
		rule.append(oldRule);
		return true;
	}

	function discard()
	{
		stack.pop();
		rule = stack[stack.length - 1];
		return true;
	}

	function finish()
	{
		while (stack.length > 1)
		{
			pop();
		}
		return true;
	}

	function parse()
	{
		while (_parse() == null)
		{}
	}

	function _parse():Stylesheet
	{
		consume();
		switch(mode) {
			case Mode.TopLevel:

				switch(token.tokenType)
				{
					case TokenType.CDO, TokenType.CDC, TokenType.WHITESPACE: //trace("skip");
					case TokenType.AT_KEYWORD: push(new AtRule(token.value)) && switchTo(Mode.AtRule);
					case TokenType.CURLY_OPEN: parseError("Attempt to open a curly-block at top-level.") && consumeAPrimitive() != null;
					case TokenType.EOF_HX: finish(); return stylesheet;
					default: push(new StyleRule()) && switchTo(Mode.Selector) && reprocess();
				}


			case Mode.AtRule:
				switch(token.tokenType)
				{
					case TokenType.SEMICOLON: pop() && switchTo();
					case TokenType.CURLY_OPEN:
						if (rule.fillType != null) switchTo(rule.fillType);
						else parseError("Attempt to open a curly-block in a statement-type at-rule.") && discard() && switchTo(Mode.NextBlock) && reprocess();
					case TokenType.EOF_HX: finish(); return stylesheet;
					default: cast(rule, AtRule).appendPrelude(consumeAPrimitive());
				}


			case Mode.Rule:
				switch(token.tokenType)
				{
					case TokenType.WHITESPACE:
					case TokenType.CURLY_CLOSE: pop() && switchTo();
					case TokenType.AT_KEYWORD: push(new AtRule(token.value)) && switchTo(Mode.AtRule);
					case TokenType.EOF_HX: finish(); return stylesheet;
					default: push(new StyleRule()) && switchTo(Mode.Selector) && reprocess();
				}


			case Mode.Selector:
				switch(token.tokenType)
				{
					case TokenType.CURLY_OPEN: switchTo(Mode.Declaration);
					case TokenType.EOF_HX: discard() && finish(); return stylesheet;
					default: cast(rule, StyleRule).appendSelector(consumeAPrimitive());
				}


			case Mode.Declaration:
				switch(token.tokenType)
				{
					case TokenType.WHITESPACE, TokenType.SEMICOLON:
					case TokenType.CURLY_CLOSE: pop() && switchTo();
					// Bug from original implementation
					case TokenType.AT_KEYWORD: push(new AtRule(token.value)) && switchTo(Mode.AtRule);
					case TokenType.IDENTIFIER: push(new Declaration(token.value)) && switchTo(Mode.AfterDeclarationName);
					case TokenType.EOF_HX: finish(); return stylesheet;
					default: parseError() && discard() && switchTo(Mode.NextDeclaration);
				}


			case Mode.AfterDeclarationName:
				switch(token.tokenType)
				{
					case TokenType.WHITESPACE:
					case TokenType.COLON: switchTo(Mode.DeclarationValue);
					case TokenType.SEMICOLON: parseError("Incomplete declaration - semicolon after property name.") && discard() && switchTo();
					case TokenType.EOF_HX: discard() && finish(); return stylesheet;
					default: parseError("Invalid declaration - additional token after property name") && discard() && switchTo(Mode.NextDeclaration);
				}


			case Mode.DeclarationValue:
				switch(token.tokenType)
				{
					case TokenType.DELIMITER:
						if (token.value == "!" && next().tokenType == 'IDENTIFIER' && next().value.toLowerCase() == "important")
						{
							consume();
							rule.important = true;
							switchTo(Mode.DeclarationEnd);
						}
						else
						{
							rule.append(token);
						}

					case TokenType.SEMICOLON: pop() && switchTo();
					case TokenType.CURLY_CLOSE: pop() && pop() && switchTo();
					case TokenType.EOF_HX: finish(); return stylesheet;
					default: rule.append(consumeAPrimitive());
				}


			case Mode.DeclarationEnd:
				switch(token.tokenType)
				{
					case TokenType.WHITESPACE:
					case TokenType.SEMICOLON: pop() && switchTo();
					case TokenType.CURLY_CLOSE: pop() && pop() && switchTo();
					case TokenType.EOF_HX: finish(); return stylesheet;
					default: parseError("Invalid declaration - additional token after !important.") && discard() && switchTo(Mode.NextDeclaration);
				}


			case Mode.NextBlock:
				switch(token.tokenType)
				{
					case TokenType.CURLY_OPEN: consumeAPrimitive() != null && switchTo(); //break;
					case TokenType.EOF_HX: finish(); return stylesheet;
					default: consumeAPrimitive(); //break;
				}


			case Mode.NextDeclaration:
				switch(token.tokenType)
				{
					case TokenType.SEMICOLON: switchTo(Mode.Declaration); //break;
					case TokenType.CURLY_CLOSE: switchTo(Mode.Declaration) && reprocess(); //break;
					case TokenType.EOF_HX: finish(); return stylesheet;
					default: consumeAPrimitive(); //break;
				}

			#if !haxe3
			default:
				// If you hit this, it's because one of the switchto() calls is typo'd.
				trace('Unknown parsing mode: ' + mode);
			#end

		}

		return null;
	}

	function consumeAPrimitive():Dynamic
	{
		switch(token.tokenType)
		{
			case TokenType.PARENTHESIS_OPEN, TokenType.CURLY_OPEN, TokenType.SQUARE_OPEN: return consumeASimpleBlock();
			case TokenType.FUNCTION: return consumeAFunc();
			default: return token;
		}
	}

	function consumeASimpleBlock()
	{
		var endingTokenType:TokenType = getEndingToken(token.tokenType);
		//trace(endingTokenType);
		var block = new SimpleBlock(token.tokenType);

		while (true)
		{
			consume();
			switch(token.tokenType)
			{
				case TokenType.EOF_HX:

				default: //block.append(consumeAPrimitive());
					if (token.tokenType == endingTokenType)
					{

						//trace("got matching end token for " + token.tokenType);
						return block;
					}
					else
					{
						//trace("appending to block " + token.tokenType);
						block.append(consumeAPrimitive());
					}
			}
		}
	}

	public static function getEndingToken(tokenType:TokenType):TokenType
	{
		return switch(tokenType)
		{
			case TokenType.PARENTHESIS_OPEN: TokenType.PARENTHESIS_CLOSE;
			case TokenType.SQUARE_OPEN: TokenType.SQUARE_CLOSE;
			case TokenType.CURLY_OPEN: TokenType.CURLY_CLOSE;
			default: throw "Invalid TokenType";
		}
	}

	function consumeAFunc()
	{
		var func = new Func(token.value);
		var arg = new FuncArg();

		while (true)
		{
			consume();
			switch(token.tokenType)
			{

				case TokenType.EOF_HX, TokenType.PARENTHESIS_CLOSE: func.append(arg); return func;

				case TokenType.DELIMITER:
					if (token.value == ",")
					{
						func.append(arg);
						arg = new FuncArg();
					}
					else
					{
						arg.append(token);
					}

				default: arg.append(consumeAPrimitive());
			}
		}

	}
}